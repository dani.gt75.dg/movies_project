export interface Products{
  cardHeaderImg: string;
}

export interface movieModel{
  name: string;
  description: string;
  image: string;
}

export interface ApiResModel{
  name: string;
  id: number;
  description: string;
  image: {
    medium: string;
  };
  res: Response;
  movieLength: number;
}

// export interface ApiResponseModel{

// }
