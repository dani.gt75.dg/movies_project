import { Component, OnInit } from '@angular/core';
import { MenuItem } from '../../models/core.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public menuList: MenuItem[] = [];


  constructor() { }

  ngOnInit(): void {
    this.menuList = [
      {
        label: 'Home',
        url: '/home',
      },
      {
        label: 'Movies',
        url: '/movies',
      },      {
        label: 'Gestion',
        url: '/gestion',
      }
    ];

  }

}
