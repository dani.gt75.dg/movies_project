import { ApiResModel } from './movie.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  constructor( private httpClient: HttpClient ) { }

  getAccesories(): Observable<any>{ //Pregunar para poder cambiar el Any
    // return this.httpClient.get('https://rickandmortyapi.com/api/character/')
    // return this.httpClient.get('https://my-json-server.typicode.com/franlindebl/shopeame-api-v2/products')
    return this.httpClient.get<ApiResModel>('https://api.tvmaze.com/shows')
      .pipe( map((response) => { //.pipe(map((response) => {} )) sirve para manipular los datos.
          const dataModel = {
            // id: response.id,
            res: response
          };
          return dataModel;
        }),
      );
  }

  getMoviesDetails(id: string): Observable<any>{
    return this.httpClient.get(`https://api.tvmaze.com/shows/${id}`)
  }
}

