export interface Movie{
  name: string;
  price: string;
  descrition: string;
  urlImage: string;
  stars: string;
  isActivated: boolean;
}
