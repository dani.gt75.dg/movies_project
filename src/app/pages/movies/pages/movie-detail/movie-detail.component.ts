import { MoviesService } from './../../store.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { movieModel } from '../../movie.model';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss']
})
export class MovieDetailComponent implements OnInit {

  movieId: movieModel;
  params$: Subscription;
  private getMoviesSubcription: Subscription;
  MoviesService: any;

  constructor( private route: ActivatedRoute ) { }

  ngOnInit(): void {
    this.params$ = this.route.paramMap.subscribe(params => {
      const userId = params.get("id");

      // this.movieId =

      console.log("user",userId);
    })
  }

  private getMovies(): void {

    this.getMoviesSubcription =  this.MoviesService.getMoviesDetails()

  }

}
