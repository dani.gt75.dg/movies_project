import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Products, movieModel, ApiResModel } from './movie.model';
import { MoviesService } from './store.service';
import {MatPaginator, PageEvent} from '@angular/material/paginator';

const re = /[^0-9]/g;

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {

  movieId: movieModel;
  cardHeaderImg: Products;
  moviesList: ApiResModel[] = [];
  movieLength: number;

  page: number = 1;
  pageSize: number = 12;
  // pageSizeOptions: number[] = [5, 10, 25, 100];
  // pageEvent: PageEvent;

  private getAccesoriesSubcription: Subscription;
  // private getMoviesDetailsSubcription: Subscription;

  constructor( private moviesService: MoviesService, private route: ActivatedRoute ) { }

  ngOnInit(): void {
    this.getAccesories();
  }

  private getAccesories(): void {
    this.getAccesoriesSubcription =  this.moviesService.getAccesories().subscribe(data => {
      this.moviesList = data.res
      this.movieLength = data.length;

      console.log('data', data)
      console.log('number data', data.length.toString())

      // console.log('data', data[0].image.medium);


      // console.log(typeof(data[0].image.medium))

    });


  }

  selectPage(page: string) {
    this.page = parseInt(page, 10) || 1;
  }

  formatInput(input: HTMLInputElement) {
    input.value = input.value.replace(re, '');
  }

  // getMoviesDetails(): void {
  //   this.getMoviesDetailsSubcription = this.moviesService.getMoviesDetails().subscribe(data => {
  //     this.moviesDetails = data;
  //   })
  // }

  ngOnDestroy(): void {
    this.getAccesoriesSubcription.unsubscribe();
  }

}
function FILTER_PAG_REGEX(FILTER_PAG_REGEX: any, arg1: string): string {
  throw new Error('Function not implemented.');
}

