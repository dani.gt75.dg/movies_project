import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Movie } from './gestion.model';

@Component({
  selector: 'app-gestion',
  templateUrl: './gestion.component.html',
  styleUrls: ['./gestion.component.scss']
})
export class GestionComponent implements OnInit {


  newMovie: Movie;


  name: string = 'Name Movie';
  price: string = '0.00€'
  description: string = 'Esto es una descripción';
  stars: string = '';
  link: string = 'https://res.cloudinary.com/people-matters/image/upload/f_auto,q_auto,w_800,h_800,c_lpad/v1517845732/1517845731.jpg';


  valueChangesFormName$: Subscription | undefined;
  valueChangesFormDescription$: Subscription | undefined;
  valueChangesFormStars$: Subscription | undefined;
  valueChangesFormUrlImage$: Subscription | undefined;


  public newMovieForm: FormGroup;

  public submitted: boolean = false;

  constructor( private formBuilder: FormBuilder ) { }

  ngOnInit(): void {
    this.newMovieForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(10)]],
      price: ['', [Validators.required, Validators.maxLength(10)]],
      description: ['', [Validators.required, Validators.maxLength(15)]],
      urlImage: ['', [Validators.required]],
      stars: ['', [Validators.required, Validators.maxLength(100)]]
    })


    console.log(this.newMovieForm.get('name')?.valueChanges);

    this.valueChangesFormName$ = this.newMovieForm.get('name')?.valueChanges.subscribe(value => {
      this.name = value;
      console.log( 'changes', this.name);
    });
    this.valueChangesFormDescription$ = this.newMovieForm.get('description')?.valueChanges.subscribe(value => {
      this.description = value;
      console.log( 'changes', this.name);
    });
    // this.newMovieForm.get('price')?.valueChanges.subscribe(value => {
    //   this.price = value;
    //   console.log( 'changes', this.name);
    // });
    this.valueChangesFormStars$ = this.newMovieForm.get('stars')?.valueChanges.subscribe(value => {
      this.stars = value;
      console.log( 'changes', this.name);
    })
    this.valueChangesFormUrlImage$ = this.newMovieForm.get('urlImage')?.valueChanges.subscribe(value => {
      this.link = value;
      console.log( 'changes', this.name);
    })
  }

  onSubmit(): void {
    this.submitted = true;

    console.log("onsubmit", this.newMovieForm);


    if (this.newMovieForm.valid){

      setTimeout(() => {
        const newMovie: Movie = {
          name: this.newMovieForm.get('name')?.value,
          price: this.newMovieForm.get('price')?.value,
          descrition: this.newMovieForm.get('description')?.value,
          urlImage: this.newMovieForm.get('urlImage')?.value,
          stars: this.newMovieForm.get('stars')?.value,
          isActivated: false,
        };
        console.log(newMovie);

        this.newMovieForm.reset();

      }, 3000);
    }
    this.submitted = false;
  }

  ngOnDestroy(){
    this.valueChangesFormName$?.unsubscribe();
    this.valueChangesFormDescription$?.unsubscribe();
    this.valueChangesFormStars$?.unsubscribe();
    this.valueChangesFormUrlImage$?.unsubscribe();
  }

}
